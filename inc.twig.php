<?php
// On importe l'autloader permettant de charger nos pages
include_once('lib/Twig/Autoloader.php');

Twig_Autoloader::register();

// On pose notre page root sur templates
// pour q'un utilisateur ne remonte pas dans les répertoires et fichiers
$templates = new Twig_Loader_Filesystem('templates');
$twig      = new Twig_Environment($templates);
